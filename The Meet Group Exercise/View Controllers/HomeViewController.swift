//
//  ViewController.swift
//  The Meet Group Exercise
//
//  Created by Dan Wood on 11/6/20.
//

import UIKit
import SwiftyJSON

class HomeViewController: UIViewController, UITextFieldDelegate {
    // Storyboard outlets
    @IBOutlet weak var searchInputView: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    
    // JSON data for search results
    private var searchReults:JSON?
    
    // --------------------------------------------
    // MARK: - Life Cycle Functions
    // --------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Register to listen for keyboard actions to move UI elements
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil);

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil);
    }
           
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toTweetsView" {
            // Pass the download data to the Tweet display view
            let destination = segue.destination as! TweetSearchViewController
            destination.searchResults = searchReults
        }
    }
    
    // --------------------------------------------
    // MARK: - Helper Functions
    // --------------------------------------------
    func displayAlert() {
        // Display general error message for missing search info
        let alertController = UIAlertController(title: "Error", message: "Search term invalid", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Whoops", style: .default, handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func searchTwitter() {
        // Init WebServices class
        let ws = WebServices.init()
        
        guard let searchTerm = searchInputView.text else {
            // Display alert that there was an error with the search term
            displayAlert()
            return
        }
        
        if searchTerm == "" {
            // Nothing has been entered in the search field; display error message
            displayAlert()
        } else {
            ws.twitterSearch(searchTerm: searchTerm) {isSuccess, json in
                if isSuccess {
                    self.searchReults = json as? JSON
                    // Trigger segue to Tweet display view
                    self.performSegue(withIdentifier: "toTweetsView", sender: nil)
                } else {
                    // Display alert that there was an error with the search term; in this case an error from Twitter
                    self.displayAlert()
                }
            }
        }
    }
    
    // --------------------------------------------
    // MARK: - IB Actions
    // --------------------------------------------
    @IBAction func searchAction(_ sender: Any) {
        searchTwitter()
    }
    
    // --------------------------------------------
    // MARK: - Textfield Delegate Functions
    // --------------------------------------------
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchTwitter()
        resignFirstResponder()
        return true
    }
    
    // --------------------------------------------
    // MARK: - Keyboard Notifications
    // --------------------------------------------
    @objc func keyboardWillShow(sender: NSNotification) {
         self.view.frame.origin.y = -150 // Move view 150 points upward
    }

    @objc func keyboardWillHide(sender: NSNotification) {
         self.view.frame.origin.y = 0 // Move view to original position
    }
    
}

