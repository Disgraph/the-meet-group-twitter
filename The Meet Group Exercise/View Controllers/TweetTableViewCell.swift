//
//  TweetTableViewCell.swift
//  The Meet Group Exercise
//
//  Created by Dan Wood on 11/6/20.
//

import UIKit

class TweetTableViewCell: UITableViewCell {

    @IBOutlet weak var cellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
