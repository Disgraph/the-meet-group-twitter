//
//  TweetSearchViewController.swift
//  The Meet Group Exercise
//
//  Created by Dan Wood on 11/7/20.
//

import UIKit
import SwiftyJSON

class TweetSearchViewController: UITableViewController {
    
    // JSON data for twitter search
    public var searchResults:JSON?
    
    // --------------------------------------------
    // MARK: - Life Cycle Functions
    // --------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the seperator line color TMG blue
        tableView.separatorColor = UIColor(red: 57.0/255.0, green: 184.0/255.0, blue: 214.0/255.0, alpha: 1.00)
    }
    
    // --------------------------------------------
    // MARK: - Table View Functions
    // --------------------------------------------
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let jsonData = searchResults else {
            print("Error unwrapping data")
            return 0
        }
        
        return jsonData["statuses"].count
    }
   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Use the custom TweetTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "tweetCell", for: indexPath) as! TweetTableViewCell

        guard let jsonData = searchResults else {
            print("Error unwrapping data")
            return cell
        }
        // Set display properties for table cell
        cell.cellLabel.text = jsonData["statuses"][indexPath.row]["text"].stringValue
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // Set custom height for table view cells
        return 120
    }

}
