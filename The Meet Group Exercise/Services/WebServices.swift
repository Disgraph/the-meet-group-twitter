//
//  WebServices.swift
//  The Meet Group Exercise
//
//  Created by Dan Wood on 11/7/20.
//

import Foundation
import Alamofire
import SwiftyJSON

class WebServices {
    
    private let twitterSearchUrl = "https://api.twitter.com/1.1/search/tweets.json"
    private let bearerToken = "AAAAAAAAAAAAAAAAAAAAAKE6%2FAAAAAAAPngA41v0Vq6LBDdbZYF0GO8dx1I%3Dbj0iNtEhMclfNHPqxbxIcu9ZvvoefYi87bvw4U7pbeJefFnfjZ"
    
    public func twitterSearch(searchTerm:String, _ completion: @escaping (_ isSuccess:Bool, _ json: Any?)->()) {
        // Set parameters for search
        let params = ["q":searchTerm,
                      "count":"20"]
        // Set headers for search
        let headers: HTTPHeaders = ["authorization":"bearer AAAAAAAAAAAAAAAAAAAAAKE6%2FAAAAAAAPngA41v0Vq6LBDdbZYF0GO8dx1I%3Dbj0iNtEhMclfNHPqxbxIcu9ZvvoefYi87bvw4U7pbeJefFnfjZ",
                       "content-type":"application/json"]
        // Make web request using Alamofire
        AF.request(twitterSearchUrl, method: .get, parameters: params, headers: headers) .response { (response) in
            // Process results from web call
            switch response.result {
            case .success(let value):
                guard let valueData = value else {
                    print("Error unwrapping downloaded data")
                    // Return that data couldn't be processed
                    completion(false, nil)
                    return
                }
                // Convert response to JSON
                let twitterJSON = JSON(valueData)
                // Respond with download as a success and with downloaded JSON data
                completion(true, twitterJSON)
            case let .failure(error):
                // Reponsd that there was an error downloading data from Twitter
                completion(false, nil)
                print("Error \(error)) ")
            }
        }
    }
}
